import 'dart:math';
import 'package:flutter/material.dart';
import './banco_questoes.dart';
import './model_question.dart';
import './resultado.dart';
import './colors.dart';
import './quiz_widget.dart';

class TelaQuiz extends StatefulWidget {
  const TelaQuiz({Key? key}) : super(key: key);
  @override
  _TelaQuizState createState() => _TelaQuizState();
}

class _TelaQuizState extends State<TelaQuiz> {
  int question_pos = 0;
  int pontuacao = 0;
  bool btnPressed = false;
  PageController? _controller;
  String btnText = "Próxima Questão!";
  bool answered = false;

  List<QuestionModel> randomQuestions = [];

  @override
  void initState() {
    super.initState();
    _controller = PageController(initialPage: 0);
    _selectedRandomQuestions();
  }

  void _selectedRandomQuestions() {
    final random = Random();
    while (randomQuestions.length < 10) {
      final randomIndex = random.nextInt(question.length);
      final randomQuestion = question[randomIndex];
      if (!randomQuestions.contains(randomQuestion)) {
        randomQuestions.add(randomQuestion);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Quiz Bíblico'),
        centerTitle: true,
      ),
      backgroundColor: Theme.of(context).colorScheme.background,
      body: Padding(
          padding: const EdgeInsets.all(18.0),
          child: PageView.builder(
            controller: _controller!,
            onPageChanged: (page) {
              if (page == 9) {
                setState(() {
                  btnText = "Veja o Resultado";
                });
              }
            },
            //physics: new NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              final QuestionModel question = randomQuestions[index];
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                      width: double.infinity,
                      child: Text('Questão ${index + 1}/10',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 28,
                          ))),
                  Divider(
                    color: Colors.white,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Flexible(
                    child: SizedBox(
                      width: double.infinity,
                      child: Text(
                        "${question.question}",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 24.0,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  for (int i = 0;
                      i < randomQuestions[index].answers!.length;
                      i++)
                    Container(
                      width: double.infinity,
                      height: 50.0,
                      margin: EdgeInsets.only(
                          bottom: 20.0, left: 12.0, right: 12.0),
                      child: RawMaterialButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        fillColor: btnPressed
                            ? randomQuestions[index].answers!.values.toList()[i]
                                ? Colors.green
                                : Colors.red
                            : FancyAppColor().secondaryColorDark,
                        onPressed: !answered
                            ? () {
                                if (randomQuestions[index]
                                    .answers!
                                    .values
                                    .toList()[i]) {
                                  pontuacao++;
                                  print("Sim");
                                } else {
                                  print("Não");
                                }
                                setState(() {
                                  btnPressed = true;
                                  answered = true;
                                });
                              }
                            : null,
                        child: Text(
                            randomQuestions[index].answers!.keys.toList()[i],
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                            )),
                      ),
                    ),
                  SizedBox(
                    height: 40.0,
                  ),
                  RawMaterialButton(
                    onPressed: () {
                      if (_controller!.page?.toInt() ==
                          randomQuestions.length - 1) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Resultado(pontuacao)));
                      } else {
                        _controller!.nextPage(
                            duration: Duration(milliseconds: 250),
                            curve: Curves.easeInExpo);

                        setState(() {
                          btnPressed = false;
                        });
                      }
                    },
                    shape: StadiumBorder(),
                    fillColor: Colors.blue,
                    padding: EdgeInsets.all(18.0),
                    elevation: 0.0,
                    child: Text(
                      btnText,
                      style: TextStyle(color: Colors.white),
                    ),
                  )
                ],
              );
            },
            itemCount: randomQuestions.length,
          )),
    );
  }
}
