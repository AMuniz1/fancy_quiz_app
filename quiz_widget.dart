import 'package:fancy_quiz_app/colors.dart';
import 'package:flutter/material.dart';
import './model_question.dart';
import './colors.dart';

Widget questionWidget(
  String question,
  Map<String, bool> answers,
  void Function() function,
  bool pressed,
) {
  return Column(
    children: [
      SizedBox(
        width: double.infinity,
        height: 200.0,
        child: Text(
          question,
          style: TextStyle(
            color: Colors.white,
            fontSize: 22.0,
          ),
        ),
      ),
      for (var answer in answers.entries)
        Container(
          width: double.infinity,
          height: 50.0,
          margin: EdgeInsets.only(bottom: 20.0, left: 12.0, right: 12.0),
          child: RawMaterialButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            fillColor: FancyAppColor().secondaryColorDark,
            onPressed: function,
            child: Text(answer.key),
          ),
        )
    ],
  );
}
