import './model_question.dart';

List<QuestionModel> question = [
  QuestionModel(
    "Qual das seguintes opções é um exemplo de uma fonte renovável de energia?",
    {
      "Carvão mineral": false,
      "Petróleo": false,
      "Energia solar": true,
      "Gás natural": false,
    },
  ),
  QuestionModel(
      "Qual é a função principal dos pulmões no sistema respiratório humano?", {
    "Transportar oxigênio para o corpo": false,
    "Remover dióxido de carbono do corpo": true,
    "Produzir oxigênio para o corpo": false,
    "Regular a pressão sanguínea": false,
  }),
  QuestionModel(
      "Qual é o processo pelo qual as plantas produzem seu próprio alimento?", {
    "Respiração": false,
    "Fotossíntese": true,
    "Digestão": false,
    "Excreção": false,
  }),
  QuestionModel("Qual é o nome da camada externa da Terra?", {
    "Núcleo": false,
    "Manto": false,
    "Atmosfera": false,
    "Crosta": true,
  }),
  QuestionModel(
      "Qual é o nome do processo em que uma substância passa do estado líquido para o estado gasoso?",
      {
        "Evaporação": true,
        "Solidificação": false,
        "Condensação": false,
        "Sublimação": false,
      }),
  QuestionModel("Qual é a unidade básica da vida?", {
    "Átomo": false,
    "Célula": true,
    "Molécula": false,
    "Organismo": false,
  }),
  QuestionModel(
      "Qual é o nome do processo pelo qual a água passa do estado gasoso para o estado líquido?",
      {
        "Evaporação": false,
        "Sublimação": false,
        "Condensação": true,
        "Solidificação": false,
      }),
  QuestionModel("Qual apóstolo nos exortou a 'abster-se de sangue'", {
    "Tiago": true,
    "Barnabé": false,
    "Pedro": false,
    "Paulo": false,
  }),
  QuestionModel("Qual é a principal função dos rins no corpo humano?", {
    "Produzir hormônios": false,
    "Eliminar resíduos do corpo": true,
    "Regular a temperatura do corpo": false,
    "Transportar nutrientes para as células": false,
  }),
  QuestionModel(
      "Qual é a camada atmosférica que contém a maioria dos gases responsáveis pelo efeito estufa?",
      {
        "Estratosfera": false,
        "Mesosfera": false,
        "Termosfera": false,
        "Troposfera": true,
      }),
  QuestionModel(
      "Qual é o nome do processo pelo qual um organismo se adapta ao ambiente ao longo do tempo?",
      {
        "Evolução": true,
        "Adaptação": false,
        "Seleção Natural": false,
        "Variação genética": false,
      }),
];
