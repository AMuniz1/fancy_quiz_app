import 'dart:ui';

class FancyAppColor {
  final primaryColorLight = const Color(0xFFFFFFFF);
  final secondaryColorLight = const Color.fromARGB(255, 35, 187, 43);
  final primaryColorDark = const Color(0x00000000);
  final secondaryColorDark = const Color.fromARGB(127, 50, 50, 127);
}
