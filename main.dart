import 'package:flutter/material.dart';
import './menu_principal.dart';

void main() {
  runApp(const MyFancyApp());
}

class MyFancyApp extends StatelessWidget {
  const MyFancyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: const MenuPrincipal(),
    );
  }
}
