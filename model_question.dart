class QuestionModel {
  final String? question;
  Map<String, bool>? answers;
  QuestionModel(this.question, this.answers);
}
